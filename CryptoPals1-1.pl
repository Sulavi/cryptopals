use strict;
use warnings;
use Math::Round qw(nearest_floor);
use Crypt::OpenSSL::AES;

sub println { if(exists $_[0]) {print $_[0] . "\n";} else{print "\n";}}

our %HexToBin = ("0"=>"0000","1"=>"0001","2"=>"0010","3"=>"0011","4"=>"0100","5"=>"0101","6"=>"0110","7"=>"0111",
                 "8"=>"1000","9"=>"1001","a"=>"1010","b"=>"1011","c"=>"1100","d"=>"1101","e"=>"1110","f"=>"1111");
        
our %BinToHex = reverse %HexToBin;

        
our %B64ToBin = ("A"=>"000000","B"=>"000001","C"=>"000010","D"=>"000011","E"=>"000100","F"=>"000101","G"=>"000110","H"=>"000111",
                 "I"=>"001000","J"=>"001001","K"=>"001010","L"=>"001011","M"=>"001100","N"=>"001101","O"=>"001110","P"=>"001111",
                 "Q"=>"010000","R"=>"010001","S"=>"010010","T"=>"010011","U"=>"010100","V"=>"010101","W"=>"010110","X"=>"010111",
                 "Y"=>"011000","Z"=>"011001","a"=>"011010","b"=>"011011","c"=>"011100","d"=>"011101","e"=>"011110","f"=>"011111",
                 "g"=>"100000","h"=>"100001","i"=>"100010","j"=>"100011","k"=>"100100","l"=>"100101","m"=>"100110","n"=>"100111",
                 "o"=>"101000","p"=>"101001","q"=>"101010","r"=>"101011","s"=>"101100","t"=>"101101","u"=>"101110","v"=>"101111",
                 "w"=>"110000","x"=>"110001","y"=>"110010","z"=>"110011","0"=>"110100","1"=>"110101","2"=>"110110","3"=>"110111",
                 "4"=>"111000","5"=>"111001","6"=>"111010","7"=>"111011","8"=>"111100","9"=>"111101","+"=>"111110","/"=>"111111");
        
our %BinTo64 = reverse %B64ToBin;

# Subset of standard ASCII, should be expanded
our %ASCIIToBin = ("A"=>"01000001","B"=>"01000010","C"=>"01000011","D"=>"01000100","E"=>"01000101","F"=>"01000110","G"=>"01000111","H"=>"01001000",
                   "I"=>"01001001","J"=>"01001010","K"=>"01001011","L"=>"01001100","M"=>"01001101","N"=>"01001110","O"=>"01001111","P"=>"01010000",
                   "Q"=>"01010001","R"=>"01010010","S"=>"01010011","T"=>"01010100","U"=>"01010101","V"=>"01010110","W"=>"01010111","X"=>"01011000",
                   "Y"=>"01011001","Z"=>"01011010","a"=>"01100001","b"=>"01100010","c"=>"01100011","d"=>"01100100","e"=>"01100101","f"=>"01100110",
                   "g"=>"01100111","h"=>"01101000","i"=>"01101001","j"=>"01101010","k"=>"01101011","l"=>"01101100","m"=>"01101101","n"=>"01101110",
                   "o"=>"01101111","p"=>"01110000","q"=>"01110001","r"=>"01110010","s"=>"01110011","t"=>"01110100","u"=>"01110101","v"=>"01110110",
                   "w"=>"01110111","x"=>"01111000","y"=>"01111001","z"=>"01111010","0"=>"00110000","1"=>"00110001","2"=>"00110010","3"=>"00110011",
                   "4"=>"00110100","5"=>"00110101","6"=>"00110110","7"=>"00110111","8"=>"00111000","9"=>"00111001"," "=>"00100000",'\''=>"00100111",
                   '!'=>"00100001",'"'=>"00100010",'#'=>"00100011",'$'=>"00100100",'%'=>"00100101",'&'=>"00100110",'('=>"00101000",')'=>"00101001",
                   '*'=>"00101010",'+'=>"00101011",','=>"00101100",'-'=>"00101101",'.'=>"00101110",'/'=>"00101111",':'=>"00111010",';'=>"00111011",
                   '<'=>"00111100",'='=>"00111101",'>'=>"00111110",'?'=>"00111111",'@'=>"01000000",'^'=>"01011110",'_'=>"01011111",'`'=>"01100000");
                   #'\n'=>"00001010");
                   
our %BinToASCII = reverse %ASCIIToBin;

our %LettToNum;
our $number = 0;
foreach('A'..'Z'){
   $LettToNum{$_} = $number;
   $number++;
}
foreach('a'..'z'){
   $LettToNum{$_} = $number;
   $number++;
}

our %NumToLett = reverse %LettToNum;

# First Parameter: number in decimal
# Second Parameter: number of bits
# Ex: BuildBitstring(5, 6) = 000101
sub BuildBitstring{
   my $num = $_[0];
   my $bitNum = $_[1];
   my $bitstring = "";
   for(my $i = 0; $i < $bitNum; $i++){
      if($num % 2 == 1) {$bitstring = "1" . $bitstring;}
      else              {$bitstring = "0" . $bitstring;}
      $num = nearest_floor(1, $num/2);
   }
   return $bitstring;
}

sub HexToBinString{
   my $hex = $_[0];
   my $bin = "";
   foreach my $c (split //, $hex){
      $bin = $bin . $HexToBin{$c};
   }
   return $bin;       
}

sub BinToHexString{
   my $bin = $_[0];
   my $hex = "";
   my $len = length($bin) % 4;
   if($len != 0) {
      for(my $i = $len; $i < 4; $i++){ $bin = $bin . "0";}
      println "length error";
   }
   foreach my $b (unpack("(A4)*", $bin)){
      $hex = $hex . $BinToHex{$b};
   }
   return $hex;
}

sub B64ToBinString{
   my $B64 = $_[0];
   my $bin = "";
   foreach my $c (split //, $B64){
      $bin = $bin . $B64ToBin{$c};
   }
   return $bin;
}

sub BinToB64String{
   my $bin = $_[0];
   my $six = "";
   my $len = length($bin) % 6;
   if($len != 0) {
      for(my $i = $len; $i < 6; $i++){ $bin = "0" . $bin;}
   }
   foreach my $b (unpack("(A6)*", $bin)){
      $six = $six . $BinTo64{$b};
   }
   return $six;
}

#If the ASCII value is not defined, changes the char to ~
sub BinToASCIIString{
   my $bin = $_[0];
   my $ascii = "";
   my $len = length($bin) % 8;
   if($len != 0) {
      for(my $i = $len; $i < 8; $i++){ $bin = "0" . $bin;}
   }
   foreach my $b (unpack("(A8)*", $bin)){
      if(exists $BinToASCII{$b}){
         $ascii = $ascii . $BinToASCII{$b};
      } else{
         $ascii = $ascii . "~";
      }
   }
   return $ascii;
}

sub ASCIIToBinString{
   my $ascii = $_[0];
   my $bin = "";
   foreach my $c (split //, $ascii){
      $bin = $bin . $ASCIIToBin{$c};
   }
   return $bin;
}

#Takes two bits as characters
sub XOR{
   if($_[0] == $_[1]){return "0"}
   else {return "1"}
}

#Takes two binary strings
sub XORstring{
   my $bin1 = $_[0];
   my $bin2 = $_[1];
   my $binOut = "";
   while(length $bin1 > length $bin2) {$bin2 = "0" . $bin2;}
   while(length $bin1 < length $bin2) {$bin1 = "0" . $bin1;} 
   for(my $i = 0; $i < length $bin1; $i = $i+1){
      $binOut = $binOut . XOR(substr($bin1,$i,1), substr($bin2,$i,1));
   }
   return $binOut;
}

sub FreqScore{
   my $str = $_[0];
   my $score = 0;
   my $freq = 0;
   
   my %EngFreq = ("A"=>0.0651738,"B"=>0.0124248,"C"=>0.0217339,"D"=>0.0349835,"E"=>0.1041442,"F"=>0.0197881,"G"=>0.0158610,"H"=>0.0492888,
                  "I"=>0.0558094,"J"=>0.0009033,"K"=>0.0050529,"L"=>0.0331490,"M"=>0.0202124,"N"=>0.0564513,"O"=>0.0596302,"P"=>0.0137645,
                  "Q"=>0.0008606,"R"=>0.0497563,"S"=>0.0515760,"T"=>0.0729357,"U"=>0.0225134,"V"=>0.0082903,"W"=>0.0171272,"X"=>0.0013692,
                  "Y"=>0.0145984,"Z"=>0.0007836," "=>0.1918182);
   
   for(my $i = 0; $i < 26; $i++){
      $freq = 0;
      foreach my $c (split //, $str){
         if($c eq $NumToLett{$i} || $c eq $NumToLett{$i+26}){
            $freq++;
         }
      }
      $freq = $freq/(length $str);
      $score = $score + ($EngFreq{$NumToLett{$i}} * $freq);
   }   
   #Special case for space 
   $freq = 0;
   foreach my $c (split //, $str){
      if($c eq " "){
         $freq++;
      }elsif (!exists $ASCIIToBin{$c}){
         $score -= 0.01;
      }
   }
   $freq = $freq/(length $str);
   $score = $score + ($EngFreq{" "} * $freq);          
   return $score;
}

# Argument 1: ASCII string to encrypt
# Argument 2: ASCII Key
sub XOREncrypt{
   my $msg = ASCIIToBinString($_[0]);
   my $key = ASCIIToBinString($_[1]);
   my $xorKey = "";
   
   #Build Key
   while(length $msg > length $xorKey){
      $xorKey = $xorKey . $key;
   }
   while(length $xorKey > length $msg){
      chop $xorKey;
   }
   my $encryptedBin = XORstring($xorKey, $msg);
   
   return BinToHexString($encryptedBin);
}

#Takes two strings and returns the number of differing bits.
sub HamDist{
   #my $bin1 = ASCIIToBinString($_[0]);
   #my $bin2 = ASCIIToBinString($_[1]);
   my $bin1 = HexToBinString($_[0]);
   my $bin2 = HexToBinString($_[1]);
   my $diff = 0;
   
   if(length $bin1 != length $bin2) {die "Mismatching string lengths"};
   
   for(my $i = 0; $i < length $bin1; $i++){
      if(substr($bin1,$i,1) ne substr($bin2,$i,1)){
         $diff++;
      }
   }
   return $diff;
}

# 1-1: Convert hex to base64
sub S1C1{
   print BinToB64String(HexToBinString("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d")) . "\n";
}

#1-2: Fixed XOR
sub S1C2 {
   my $bin1 = HexToBinString("1c0111001f010100061a024b53535009181c");
   my $bin2 = HexToBinString("686974207468652062756c6c277320657965");
   print BinToHexString(XORstring($bin1, $bin2))  . "\n";
}

#1-3: Single-byte XOR cipher
sub S1C3 {
   my $byte = "";
   my $winnerIndex = 0;
   my $winnerValue = 0;
   my $winnerString = "winner string";
   my $bin = HexToBinString($_[0]);
   #for(my $i = 0; $i < 52; $i++){
   for(my $i = 0; $i < 255; $i++){
      #build byte
      $byte = "";
      for(my $j = 0; $j < (length $_[0])/2; $j++){
         #$byte = $byte . $ASCIIToBin{$NumToLett{$i}};
         $byte = $byte . BuildBitstring($i,8);
      }
      # println $byte;
      # println $bin;
      
      my $xor = XORstring($byte,$bin);
      my $xorASCII = BinToASCIIString($xor);
      
      #Evaluate String
      my $value = FreqScore($xorASCII);
      #println $value . "\t" . $xorASCII;

      if($value > $winnerValue){
         $winnerValue = $value;
         $winnerIndex = $i;
         $winnerString = $xorASCII;
      }
   }
   
   #println "Winner: " . $NumToLett{$winnerIndex} . " at " . $winnerValue . " points"; 
   $byte = "";
   for(my $j = 0; $j < (length $_[0])/2; $j++){
      $byte = $byte . BuildBitstring($winnerIndex,8);
   }
   #println  BinToASCIIString(XORstring($byte,$bin));
   return BinToASCIIString(XORstring($byte,$bin));
}

#println S1C3("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736");
#println S1C3("7b5a4215415d544115415d5015455447414c155c46155f4058455c5b523f");


sub S1C4{
   my @xorStrings;
   my $index = 0;
   my $string = "";
   my $stringScore = 0;
   
   my $filename  = "Challenge4Strings.txt";
   open(my $fh, '<:encoding(UTF-8)', $filename)
      or die "Could not open file";
    
    
   while (my $row = <$fh>){
      chomp $row;
      $xorStrings[$index] = $row;
      $index++;
   }
   
   my $num = 0;
   foreach my $ln (@xorStrings){
   #for(my $k = 170; $k < 182; $k++){
      #my $ln = $xorStrings[$k];
      #println $num;
      $num++;
      my $str = S1C3($ln);
      if(FreqScore($str) > $stringScore){
         $string = $str;
         $stringScore = FreqScore($str);
      }
      #<>;
      
     #if(FreqScore($str) > 0){println FreqScore($str) . "\t" . $str;}
     #println FreqScore($str) . "\t" . $str;
   }
   println $string;
}

#S1C4;

sub S1C5{
   my $message = "Burning 'em, if you ain't quick and nimble\\nI go crazy when I hear a cymbal";
   println XOREncrypt($message, "ICE");
}

#S1C5;

sub S1C6{
   # Open File
   my $text;
   my $filename  = "Challenge6File.txt";
   #my $filename  = "toDecodeTest.txt";
   
   open(my $fh, '<:encoding(UTF-8)', $filename)
      or die "Could not open file";
        
   while (my $row = <$fh>){
      chomp $row;
      $text = $text . $row;
   }
   $text = BinToHexString(B64ToBinString($text));
   
   # Find keysize
   my $keysize;
   my $ham = 1000;
   for(my $i = 2; $i < 40; $i++){
      my $tmpham1 = HamDist(substr($text, 0, $i*2), substr($text, $i*2, $i*2));
      my $tmpham2 = HamDist(substr($text, 2*$i*2, $i*2), substr($text, 3*$i*2, $i*2));
      my $tmpham3 = HamDist(substr($text, 4*$i*2, $i*2), substr($text, 5*$i*2, $i*2));
      my $tmpham4 = HamDist(substr($text, 6*$i*2, $i*2), substr($text, 7*$i*2, $i*2));
      my $tmphamAvg = ($tmpham1 + $tmpham2 + $tmpham3 + $tmpham4)/(4*$i);
      if(($tmphamAvg) < $ham){
         $ham = $tmphamAvg;
         $keysize = $i;
      }
      println "Keysize is: " . $i . "\t" . $tmphamAvg;
   }

   $keysize = 29;
   println "Keysize: " . $keysize;

   # Split into blocks;
   my @blocks;
   my $blockIndex = 0;
   for(my $i = 0; $i < length($text); $i+=2){
      $blocks[$blockIndex] .= substr($text, $i, 2);
      $blockIndex++;
      if($blockIndex == $keysize){$blockIndex = 0}
   }
   
   # Single Bye decrypy each block
   foreach my $str (@blocks){
      $str = S1C3($str);
      #println $str;
   }
   println;
   
   # Recombine blocks
   my $output = "";
   for(my $i = 0; $i < length $blocks[0]; $i++){
      for(my $j = 0; $j < $keysize; $j++){
         $output .= substr($blocks[$j], $i, 1);
      }
   }
   
   println $output; 
   println;   
}

#S1C6;

sub S1C7{

   # Open File
   my $text;
   my $filename  = "Challenge7File.txt";
   
   open(my $fh, '<:encoding(UTF-8)', $filename)
      or die "Could not open file";
      
   while (my $row = <$fh>){
      chomp $row;
      $text = $text . $row;
   }
   
   my $cypher = new Crypt::OpenSSL::AES("YELLOW SUBMARINE");
   
   println $ciper->decrypt($text);
   
}

S1C7;










